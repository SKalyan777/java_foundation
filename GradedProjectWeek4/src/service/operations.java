package service;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import entity.BookStore;
import java.util.Scanner;
public class operations {
	Scanner sc = new Scanner(System.in);
	public void AddBook(BookStore b) {
		SessionFactory factory = null;
		 factory = new Configuration()
					.configure("hibernate.cfg.xml").addAnnotatedClass(BookStore.class)
					.buildSessionFactory();
		 Session session = factory.getCurrentSession();
		 
		try {
			 session.beginTransaction();
			 session.save(b);
			 session.getTransaction().commit();
		}catch (Exception e) {
			System.out.println(e);
		}finally {
			session.close();
		}
	}
	public void deleteBook(int id) {
		SessionFactory factory = null;
		 factory = new Configuration()
					.configure("hibernate.cfg.xml").addAnnotatedClass(BookStore.class)
					.buildSessionFactory();
		 Session session = factory.getCurrentSession();
		 session.beginTransaction();
		BookStore e = session.get(BookStore.class, id);
		try {
		if(e==null) {
			System.out.println("Book doesn't exists");
		}else {
			
					session.delete(e);
					session.getTransaction().commit();
			System.out.println("Book deleted successfully");
		}
		}catch(Exception m) {
			System.out.println(m);
		}finally {
			session.close();
		}
	}
	public void updateBook(int id) {
		System.out.println("Enter new book name");
		String nb=sc.next();
		SessionFactory factory = null;
		 factory = new Configuration()
					.configure("hibernate.cfg.xml").addAnnotatedClass(BookStore.class)
					.buildSessionFactory();
		 Session session = factory.getCurrentSession();
		 session.beginTransaction();
		BookStore e = session.get(BookStore.class, id);
		try {
		if(e==null) {
			System.out.println("Book Doesn't exists");
		}else {
			
			e.setB_name(nb);
			session.update(e);
			session.getTransaction().commit();
			System.out.println("Book Updated successfully");
		}
		}catch(Exception m) {
			System.out.println(m);
		}
		finally {
			session.close();
		}
	}
	public void updateBookPrice(int id) {
		System.out.println("Enter new price");
		float np=sc.nextFloat();
		SessionFactory factory = null;
		 factory = new Configuration()
					.configure("hibernate.cfg.xml").addAnnotatedClass(BookStore.class)
					.buildSessionFactory();
		 Session session = factory.getCurrentSession();
		 session.beginTransaction();
		BookStore e = session.get(BookStore.class, id);
		try {
		if(e==null) {
			System.out.println("Book Doesn't exists");
		}else {
			
			e.setPrice(np);
			session.update(e);
			session.getTransaction().commit();
			System.out.println("Book Updated successfully");
		}
		}catch(Exception m) {
			System.out.println(m);
		}
		finally {
			session.close();
		}
	}
	public void updateBookAuthor(int id) {
		System.out.println("Enter new Author");
		String nname=sc.next();
		SessionFactory factory = null;
		 factory = new Configuration()
					.configure("hibernate.cfg.xml").addAnnotatedClass(BookStore.class)
					.buildSessionFactory();
		 Session session = factory.getCurrentSession();
		 session.beginTransaction();
		BookStore e = session.get(BookStore.class, id);
		try {
		if(e==null) {
			System.out.println("Book Doesn't exists");
		}else {
			
			e.setA_name(nname);
			session.update(e);
			session.getTransaction().commit();
			System.out.println("Book Updated successfully");
		}
		}catch(Exception m) {
			System.out.println(m);
		}
		finally {
			session.close();
		}
	}
	public void display(){
		SessionFactory factory = null;
		 factory = new Configuration()
					.configure("hibernate.cfg.xml").addAnnotatedClass(BookStore.class)
					.buildSessionFactory();
		 Session session = factory.getCurrentSession();
		
		 try {
			 session.beginTransaction();
			 List<BookStore> books = session.createQuery("from BookStore").list();

			displayTeachers(books);
			 
			 session.getTransaction().commit();
		 }catch (Exception e) {
				System.out.println(e);
		}finally {
				session.close();
		}
		 
	}
	public void AutoBiographyGenre(){
		SessionFactory factory = null;
		 factory = new Configuration()
					.configure("hibernate.cfg.xml").addAnnotatedClass(BookStore.class)
					.buildSessionFactory();
		 Session session = factory.getCurrentSession();
		 session.beginTransaction();
		List<BookStore> Autobooks =session.createQuery(" from BookStore where genre='autobiography'").list();
		displayTeachers(Autobooks);
	}
	public void BooksCount() {
		SessionFactory factory = null;
		 factory = new Configuration()
					.configure("hibernate.cfg.xml").addAnnotatedClass(BookStore.class)
					.buildSessionFactory();
		 Session session = factory.getCurrentSession();
		 session.beginTransaction();
		List<BookStore> qry =session.createQuery("from BookStore").list();
		int count=qry.size();
		System.out.println("Books Count: "+count);


}
	private static void displayTeachers(List<BookStore> BookDetails) {
		for (BookStore BOOK : BookDetails) {
			System.out.println(BOOK);
		}
	}
}
